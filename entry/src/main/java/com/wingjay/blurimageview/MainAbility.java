/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wingjay.blurimageview;

import com.wingjay.blurimageviewlib.BlurImageView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

/**
 * 加载图片模糊进度效果示例
 */
public class MainAbility extends Ability {

    private final static String[] mediumSmUrl = {
            "https://upload-images.jianshu.io/upload_images/1825662-4c4e9bc7148749b7.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/62",
            "https://upload-images.jianshu.io/upload_images/1977600-c562e582d45a4dee.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/62",
            "https://upload-images.jianshu.io/upload_images/1761761-704c9d7ed34d112b.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/62",
            "https://upload-images.jianshu.io/upload_images/2557965-b224163bc8a4a695.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/62"
    };

    private final static String[] mediumNmUrl = {
            "https://upload-images.jianshu.io/upload_images/1825662-4c4e9bc7148749b7.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/620",
            "https://upload-images.jianshu.io/upload_images/1977600-c562e582d45a4dee.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/620",
            "https://upload-images.jianshu.io/upload_images/1761761-704c9d7ed34d112b.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/620",
            "https://upload-images.jianshu.io/upload_images/2557965-b224163bc8a4a695.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/620"
    };
    private int i = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_blur_image_layout);
        BlurImageView blurImageView = (BlurImageView) findComponentById(ResourceTable.Id_blurImageView);
        Button loadImage = (Button) findComponentById(ResourceTable.Id_loadImage);
        Button clearImage = (Button) findComponentById(ResourceTable.Id_clearImage);
        Button mediaImage = (Button) findComponentById(ResourceTable.Id_mediaImage);
        int blurFactor = BlurImageView.DEFAULT_BLUR_FACTOR;
        blurImageView.setBlurFactor(blurFactor);
        loadImage.setClickedListener(component -> {
            i++;
            if (i > 3) {
                i = 0;
            }

            blurImageView.setFullImageByUrl(mediumSmUrl[i], mediumNmUrl[i]);
        });
        mediaImage.setClickedListener(component -> blurImageView.setBlurImageByRes(ResourceTable.Media_test_pic, getContext()));
        clearImage.setClickedListener(component -> blurImageView.clear());

    }
}
