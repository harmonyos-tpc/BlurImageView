# BlurImageView for openHarmony

### Demo


### How it works?
Solution : local har package integration
1. Add the UniversalImageLoader.har package to the lib folder.
2. Add the har package to the lib folder.
3. Add the following code to gradle of the entry:
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

### method of use
```
添加中心仓库
allprojects{
    repositories{
        mavenCentral()
    }
}
添加依赖配置
implementation 'io.openharmony.tpc.thirdlib:BlurImageView:1.0.0' 
```

### use
1. First it will show user a blurry image;
2. At the same time, it starts to load the real image;
3. once loaded, replace the blurry one automatically.

## Entry Running Requirements
Use DevEco Studio and download the Harmony SDK.
Change the dependencies→classpath version in the build.gradle file to the corresponding version. (that is, the version used in your IDE's new project)


### How to use it?
```
    BlurImageView blurImageView = (BlurImageView) findComponentById(ResourceTable.Id_blurImageView);
    blurImageView.setFullImageByUrl(ANIMAL_PICTURE, ANIMAL_PICTURE);
```
   This will **load and blur** a image.
```
  BlurImageView fullBlurImageView = (BlurImageView) findComponentById(ResourceTable.Id.XXX)
  fullBlurImageView.setFullImageByUrl(blurImageUrl, normalImageUrl);
```
  This will **load two images progressively**.
```
  blurImageView.setBlurFactor(blurFactor);
```
  This can set the factor of blurry, default 8 - 10.
```
  blurImageView.setFailDrawable(Element failDrawable);
  blurImageView.setDefaultDrawable(Element defaultDrawable);
```
  Configure your own drawable for loading failure & default. Use null to remove them.
  





